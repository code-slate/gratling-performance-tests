
# Gratling Performance Tests Course

All the course video tutorials for the Gratling Performance Tests Course are on Code Slate YouTube channel.

# How to use the source files

Each lesson has it's own branch. To see the code for a lesson, select that lesson from the branch drop-down

# Start Springboot app

  Go to project directory and run following command

  `$gradle bootRun`

# Run performance tests

  Go to performance directory in the project and run following command

  `$gradle clean createEmployeeloadTest`

  
