package net.codeslate.gatling.performance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatlingPerformanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatlingPerformanceApplication.class, args);
	}

}
