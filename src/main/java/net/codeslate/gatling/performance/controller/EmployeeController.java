package net.codeslate.gatling.performance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import net.codeslate.gatling.performance.model.Employee;
import net.codeslate.gatling.performance.services.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@GetMapping(value = "/employees/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	Employee getEmployee(@PathVariable Long id) {
		System.out.println("Calling get Employees Id : "+id);
		return employeeService.getEmployee(id);
	}

	@GetMapping(value="/employees", produces = { MediaType.APPLICATION_JSON_VALUE })
	List<Employee> all() {
		return employeeService.getAllEmployees();
	}

	@PostMapping(value="/employees", produces = { MediaType.APPLICATION_JSON_VALUE })
	Employee newEmployee(@RequestBody Employee newEmployee) {
		return employeeService.addEmployee(newEmployee);
	}

}
