package net.codeslate.gatling.performance.services;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

import net.codeslate.gatling.performance.model.Employee;

@Service
public class EmployeeService {

	public static final Map<Long, Employee> employeeMap = new LinkedHashMap<Long, Employee>();

	private static long idCounter = 1000;

	public static synchronized Long createID()
	{
		return idCounter++;
	}

	static {
		Employee emp1 = new Employee(EmployeeService.createID(), "Bill Gates", "Manager");
		Employee emp2 = new Employee(EmployeeService.createID(), "Bilbo Baggins", "Developer");
		Employee emp3 = new Employee(EmployeeService.createID(), "Bill Clinton", "Director");
		Employee emp4 = new Employee(EmployeeService.createID(), "John Ried", "Team Lead");
		Employee emp5 = new Employee(EmployeeService.createID(), "Ronald Goldman", "Architect");
		Employee emp6 = new Employee(EmployeeService.createID(), "Kapil Sharma", "Designer");
		employeeMap.put(emp1.getId(), emp1);
		employeeMap.put(emp2.getId(), emp2);
		employeeMap.put(emp3.getId(), emp3);
		employeeMap.put(emp4.getId(), emp4);
		employeeMap.put(emp5.getId(), emp5);
		employeeMap.put(emp6.getId(), emp6);
	}

	public List<Employee> getAllEmployees() {
		return List.copyOf(employeeMap.values());
	}
	
	public Employee getEmployee(Long id) {
		return employeeMap.get(id);
	}

	public Employee addEmployee(Employee employee) {
		employee.setId(EmployeeService.createID());
		System.out.println("Employee to Create: "+employee);
		employeeMap.put(employee.getId(), employee);
		return employee;
	}

	public void deleteById(long id) {
		employeeMap.remove(id);
	}
	

}
