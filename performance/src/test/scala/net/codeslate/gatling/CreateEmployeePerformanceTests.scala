package net.codeslate.gatling

import com.typesafe.config.{Config, ConfigFactory}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import net.codeslate.gatling.request.Requests

import scala.language.postfixOps
import scala.concurrent.duration._

class CreateEmployeePerformanceTests extends Simulation {

  val config = ConfigFactory.load
  private val employeeFeeder = csv("employee-feeder.csv").circular

  setUp(
    scenario("create-employee-scenario")
      .feed(employeeFeeder)
      .exec(Requests.createEmployee)
      .inject(
        rampUsers(config.getInt("num-users")) during
          (config.getInt("user-ramp-seconds") seconds)))
    .maxDuration(config.getInt("test-duration-seconds") seconds)
    .protocols(
      http
        .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationJson)
        .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationJson)
        .baseUrl(s"${config.getString("host")}")
    )
}

