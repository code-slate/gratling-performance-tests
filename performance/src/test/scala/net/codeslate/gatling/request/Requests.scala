package net.codeslate.gatling.request

import com.fasterxml.jackson.databind.ObjectMapper
import com.typesafe.config.{Config, ConfigFactory}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder
import net.codeslate.gatling.performance.model.Employee

object Requests {

  val config: Config = ConfigFactory.load

  val getEmployee: HttpRequestBuilder = http("get-employee-by-id")
    .get(config.getString("uri-get-employee")+ "/" + "${employeeId}")
    .check(status.is(200))

  val healthCheck: HttpRequestBuilder = http("health-check")
    .get(config.getString("uri-health"))
    .check(status.is(200))


  val createEmployee : HttpRequestBuilder = http("create-employee")
    .post(config.getString("uri-employees"))
    .body(StringBody(new ObjectMapper().writeValueAsString(new Employee("${username}","${job}")))).asJson
    .check(status.is(200))
    .check(jsonPath("$.id").saveAs("empId"))
}
